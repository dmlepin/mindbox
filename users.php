<?php
ini_set('memory_limit', '4GB');

require_once('mindbox.class.php');
require_once ('areas.php');


// Proxy Object
class KafkaConsumer
{
    
}

$conf = new RdKafka\Conf();

$conf->setRebalanceCb(function (RdKafka\KafkaConsumer $kafka, $err, array $partitions = null) {
    switch ($err) {
        case RD_KAFKA_RESP_ERR__ASSIGN_PARTITIONS:
            echo "Assign: ";
            var_dump($partitions);
            $kafka->assign($partitions);
            break;

        case RD_KAFKA_RESP_ERR__REVOKE_PARTITIONS:
            echo "Revoke: ";
            var_dump($partitions);
            $kafka->assign(NULL);
            break;

        default:
            throw new \Exception($err);
    }
});

$topicConf = new RdKafka\TopicConf();
$topicConf->set("auto.commit.interval.ms", 1e3);
$topicConf->set("offset.store.sync.interval.ms", 60e3);
$topicConf->set("offset.store.method", "file");
$topicConf->set("offset.store.path", "kafka_offset");
$topicConf->set("auto.offset.reset", 'smallest');


$rdk = new \RdKafka\Consumer();

$kafkaConnection = "PLAINTEXT://spb-pl-kfkapp01.zoloto585.int:9092, PLAINTEXT://spb-pl-kfkapp02.zoloto585.int:9092, PLAINTEXT://spb-pl-kfkapp03.zoloto585.int:9092";

$rdk->addBrokers($kafkaConnection);

$clients_topic_name = 'push.clients.accounts.prod';

$topic = $rdk->newTopic($clients_topic_name, $topicConf);
$queue = $rdk->newQueue();

//$topic->consumeStart(0, 106300);
//$topic->consumeStart(0, RD_KAFKA_OFFSET_STORED);

try {
    $topic->consumeQueueStart(0, RD_KAFKA_OFFSET_STORED, $queue);
}
catch (Exception $te){
    echo "RDF: {$te->getMessage()}\n";   
}

use MindBox\MindBox;

$mb = new MindBox();

while (true)
{
    $msg = $queue->consume(1000);
    
    if (is_object($msg))
    {
        if ($msg->err)
        {
            switch ($msg->err)
            {
                case RD_KAFKA_RESP_ERR_NO_ERROR:
                    var_dump($msg);
                    break;
                case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                    echo "No more messages; will wait for more\n";
            }
        }
        else
        {
            if(!empty($msg->payload))
            {
                $offset = $msg->offset;
                $payload = $msg->payload;
              
                $arr =  json_decode($payload, true);
                 
                try
                {
                    if (isset($arr['action']))
                    {
                        switch ($arr['action'])
                        {
                            case 'updated':
                                $client = $mb->updateUser($arr);
                                $type = 'upd';
                                break;
                                
                            case 'created':
                                $client = $mb->createUser($arr);
                                $type = 'crt';
                             default:
                             break;
                        }
                    }
                   
                   // $client = $mb->createUser($arr);
                    $ret = $mb->sendRequest();
                    
                    // callBack!
                    //$str = print_r($arr,1)."\noffset: {$offset}\n\n";
                    echo "{$offset}\t{$type}\n";
                    
                } catch (Exception $e){
                    // ��������� �� ������������ ���c���
                    file_put_contents('nwo.txt', $payload."\n\n", FILE_APPEND);
                    echo $e->GetMessage();
                }
            }
        }
    }
}



