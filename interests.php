<?php
ini_set('memory_limit', '4GB');

require_once('mindbox.class.php');
require_once ('areas.php');

$conf = new RdKafka\Conf();

$conf->setRebalanceCb(function (RdKafka\KafkaConsumer $kafka, $err, array $partitions = null) {
    switch ($err) {
        case RD_KAFKA_RESP_ERR__ASSIGN_PARTITIONS:
            echo "Assign: ";
            var_dump($partitions);
            $kafka->assign($partitions);
            break;

        case RD_KAFKA_RESP_ERR__REVOKE_PARTITIONS:
            echo "Revoke: ";
            var_dump($partitions);
            $kafka->assign(NULL);
            break;

        default:
            throw new \Exception($err);
    }
});

$topicConf = new RdKafka\TopicConf();
$topicConf->set("auto.commit.interval.ms", 1e3);
$topicConf->set("offset.store.sync.interval.ms", 60e3);
$topicConf->set("offset.store.method", "file");
$topicConf->set("offset.store.path", "kafka_offset");
$topicConf->set("auto.offset.reset", 'smallest');


$rdk = new \RdKafka\Consumer();

$kafkaConnection = "PLAINTEXT://spb-pl-kfkapp01.zoloto585.int:9092, PLAINTEXT://spb-pl-kfkapp02.zoloto585.int:9092, PLAINTEXT://spb-pl-kfkapp03.zoloto585.int:9092";

$rdk->addBrokers($kafkaConnection);

$interest_topic_name = 'push.interests.prod';

$topic = $rdk->newTopic($interest_topic_name, $topicConf);
$queue = $rdk->newQueue();

try {
    $topic->consumeQueueStart(0, RD_KAFKA_OFFSET_STORED, $queue);
}
catch (Exception $te){
    echo "RDF: {$te->getMessage()}\n";   
}

use MindBox\MindBox;

$img_host = 'https://vv.zoloto585.ru';
while (true)
{
    $msg = $queue->consume(1000);
    
    if (is_object($msg))
    {
        
        if ($msg->err)
        {
            switch ($msg->err)
            {
                case RD_KAFKA_RESP_ERR_NO_ERROR:
                    var_dump($msg);
                    break;
                case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                    echo "No more messages; will wait for more\n";
            }
        }
        else
        {
            if(!empty($msg->payload))
            {
                $offset = $msg->offset;
                $payload = $msg->payload;
              
                $arr =  json_decode($payload, true);

                if (isset($arr['status_id']))
                {
                    if ($arr['status_id'] == 2) //@todo RTOM_VIEW_INTEREST_STATUS
                    {
                        $date = $arr['timestamp'];
                        $shop_id = $arr['pos_code'];
                        
                        //@hack
                        $sap_code = trim($arr['sap_code']);
                        
                        if (strlen($sap_code) > 10){
                            $sap_code = substr($sap_code,8);
                        }
                        echo "{$sap_code}\t{$offset}\n";
                        $price = $arr['price'] - $arr['discount'];
                      
                        $u = [];
                       
                        $info = makeRequest($sap_code);
                        if (isset($info['sku'][0]))
                        {
                            $mb = new MindBox();
                            
                            $inf = $info['sku'][0];
                           
                            $img = null;
                            
                            if (isset($info['sku'][0]['images'][2]['medium'])){
                                $img = $img_host.$inf['images'][2]['medium'];
                            }
                            if (isset($info['sku'][0]['images'][1]['medium'])){
                                $img = $img_host.$inf['images'][1]['medium'];
                            }
                            if (isset($info['sku'][0]['images'][0]['medium'])){
                                $img = $img_host.$inf['images'][0]['medium'];
                            }
                            
                            $u['customer']['ids']['zoloto585CustomerGuid'] = $arr['client_id'];
                            
                            $u['viewProduct']['product']['sku']['ids']['zoloto585System'] = $sap_code;
                            $u['viewProduct']['product']['sku']['price'] = $price;
                            $u['viewProduct']['product']['sku']['name'] = $inf['name'];
                            
                            if ($img != null)
                            {
                                $u['viewProduct']['product']['sku']['pictureUrl'] = $img;
                                $u['viewProduct']['product']['pictureUrl']= $img;
                            }
                           
                            $u['viewProduct']['product']['ids']['zoloto585System'] = $sap_code;
                            $u['viewProduct']['product']['name'] = $inf['name'];
                           
                            $is_new = false;
                            if (isset($inf['attributes']['status_matrix']) && isset($inf['attributes']['order_status']))
                            {
                                if($inf['attributes']['status_matrix'] == 3 && $inf['attributes']['order_status'] == 1){
                                    $is_new = true;
                                }
                            }
                            
                            $u['viewProduct']['product']['customFields']['zoloto585ProductNewFlag'] = $is_new;
                            $u['viewProduct']['product']['price'] = $price;
                            $u['pointOfContact'] = $arr['pos_code'];
                            
                            $u['executionDateTimeUtc'] = date("d.m.Y H:i:s",time()-10800);
                            
                            $ret = $mb->createRTOMInterest($u);
                          
                        }
                     }
                }
            }
        }
    }
}



function makeRequest($saps)
{
    $req_data = '{"product_id": ['.$saps.']}';
    
   
    $uri = 'http://10.128.0.12/api/sku/info';
   
    $header = [];
    $header[] = 'Content-Type: application/json';
    $header[] = 'Accept: application/json';
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $uri);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $req_data);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     
    $try = 0;
    $pass = false;
    do {
        $data_str  = curl_exec($ch);
        $status = curl_getinfo($ch);
    
        //echo $data_str;
        $ret = json_decode($data_str, true);
        if($status["http_code"] != 200)
        {
            $ret = null;
        }
        try{
            $pass = true;
    
        }
        catch (Exception $e){
            $try++;
        }
    } while (curl_errno($ch) && $try <= 3);
    
    return $ret;
}
