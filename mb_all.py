# -*- coding: utf-8 -*-
import pyodbc as p
import json
import mb_config as c
import logging as l
from threading import Thread
from requests import post, exceptions
from datetime import datetime
from time import sleep


# определяем файл конфига
l.basicConfig(filename='/opt/Mindbox_AllOperation.log', level=l.INFO)


methods = {
    "All": ["GetAllActions", "All"]
}

def get_json(method:str, jsn:dict):
    """
    Передаем в API метод из словаря выше
    с телом запроса и возвращаем JSON
    """
    # выполняем post запрос и записываем результат в переменную
    result = post(
        c.url + method,
        json=jsn,
        verify=False,
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": c.key
        })
    print (result)
    return result.text


def exec_sql(query:str, return_anything:bool=False):
    """
    Подключаемся к базе данных MSSQL и выполняем запрос,
    а в зависимости от параметра return_anything возвращаем
    или нет список значений
    """
    # подключаемся к базе данных средствами контекстного менеджера
    with p.connect(c.con_str, autocommit=True) as con:
        try:
            # определяем курсор
            cursor = con.cursor()
            # выполняем запрос и записываем результат запроса в переменную
            exec_result = cursor.execute(query)
        # в случае ошибки выводим на экран текст ошибки
        except p.ProgrammingError as e:
            print(str(e))

        # если параметр == True - возвращаем результат запроса
        if return_anything:
            vals = exec_result.fetchall()
            value_list = [i[0] for i in vals]
            return value_list


def get_last_mindboxid(channel:str):
    """
    получаем последний идентификатор действия
    из таблицы Promo
    """
    return exec_sql("""
        select isnull(max(ActionId), 1)+1
        from dwh.mindbox.Promo
        """, True)[0]


class Promo():
    """
    Класс определяется параметром method, который мы получаем
    из словаря methods
    """
    def __init__(self, method:str):
        self.method = method[0]
        self.name = method[1]
        self.insert_string = c.insert_promo
        self.row_number = 0
        self.error_number = 0
        
        self.request_promo = {"page": {
                "itemsPerPage": 1000,
                "firstMindboxId": get_last_mindboxid(self.name)
            }}
        '''
        self.request_promo = {"page": {
                "itemsPerPage": 1000,
                "firstMindboxId": 182667435
            }}
        '''
    """
    Функция insert() в итерации обращается к API и записывает полученные данные в БД
    """
    def insert(self):
        cnt = 0
        while True:
            try:
                # записываем результат функции get_json в переменную в виде словаря
                j_data = json.loads(get_json(self.method, self.request_promo))
            # в случае ошибки SSL сертификата выводим на экран текст ошибки, логируем ошибку,
            # обнуляем переменную и запускаем функцию еще раз через 15 секунд
            except exceptions.SSLError as e:
                print('\n\t{} - SSLError\firstMindboxId: {}\n'.format(self.name, self.request_promo["page"]["firstMindboxId"]))
                l.error('{}. {}\n{}\n'.format(self.row_number, self.name, repr(e)))
                j_data = None
                sleep(5)
                self.insert()
            # если данные из API получены - проверяем статус и если он успешный
            if j_data["status"] == 'Success':
                try:
                    # пробуем получить список объектов по ключу
                    values_list = j_data["customerActions"]
                # если получаем ошибку, значит объектов нет, все данные уже получены
                # и на этом поток завершает работу
                except KeyError:
                    print('_________{}_:______complete___________'.format(self.name))
                    l.error('Upload {} is complete. Last "firstMindboxId" is {}\n'.format(
                        self.name,
                        self.request_promo["page"]["firstMindboxId"]
                        ))
                    break
            # если статус не успешный, значит есть проблемы на стороне API
            # и мы пробуем запустить всю функцию insert() снова максимум 15 раз пока не получим данные
            # если по истечению 15-и попыток данные получить не удалось - поток завершает работу
            else:
                print('\n{} status is not Success\n{}\n'.format(self.name, j_data))
                l.info('{}. {}\nrequest: {}\n{}'.format(
                    self.row_number,
                    self.method,
                    self.request_promo,
                    j_data
                    ))
                if self.error_number < 1500000000:
                    self.error_number += 1
                    j_data = None
                    self.insert()
                    break
                else:
                    print('{self.name}\tMore then {self.error_number} bad status\n')
                    l.error('{self.row_number}. {self.method} More then {self.error_number} bad status\n')
                    break

            # если мы-таки получили список действий, тогда итерационно выбираем нужные там значения,
            # размещаем их в строке для того чтобы потом соединить со строкой в которой команда
            values = ''
            for j in values_list:
                try:
                    # если в списке есть группа клиентов - записываем её в переменную
                    #groupingKey = j["mailing"]["notSentReason"]["ids"].get("CustomerInMailingControlGroup", '')
                    groupingKey = j["mailing"]["notSentReason"]["ids"].get('systemName','')

                except KeyError:
                    # если нет - оставляем пустоту
                    groupingKey = ''

                try:
                    if j["mailing"].get("channel"):
                        cnt += 1
                        channel = j["mailing"].get("channel")
                        values += "\t({}, {}, '{}', {}, '{}', '{}', '{}', {}, CONVERT(VARBINARY(MAX), '{}', 1)​),\n".format(
                        #values += "\t({}, {}, '{}', {}, '{}', '{}', '{}', {}, '{}'​),\n".format(
                            j["customer"]["ids"].get("mindboxId", -1),
                            j["mailing"].get("groupingKey", -1),
                            j["actionTemplate"].get("name", 'Empty'),
                            j["ids"].get("mindboxId", -1),
                            channel,
                            j.get("dateTimeUtc", '').replace('T', ' ')[:19],
                            groupingKey,
                            self.request_promo["page"]["firstMindboxId"],
                            j["customer"]["ids"].get("zoloto585CustomerSapId", '0x00')
                        )
                    # \u200b unicode fix
                    values = values.replace('\u200b', ' ')
                    # выполняем полученную команду в SQL Server'e, удалив предварительно посоеднюю запятую
                    #q_str = self.insert_string + values.rstrip()[:-1]


                    #exit(1)
                    # логируем факт загрузки порции данных
                    l.info('{}. {}  firstMindBoxId: {}\n'.format(
                        self.row_number,
                        self.method,
                        self.request_promo["page"]["firstMindboxId"]
                    ))
                    print (cnt)
                    # выводим этот факт на экран
                    print(self.name, values_list[-1]["ids"]["mindboxId"])
                except:
                    pass
                    #print ("pass...")
            if len(values) > 10:
                q_str = self.insert_string + values.rstrip()[:-1]
                #print (q_str)
                exec_sql(q_str)
            # если данных 1000 (максимальное количество которое мы можем получить от сервиса)
            # значит данные еще есть и мы будем обращаться к сервису за данными еще
            # для этого мы смотрим последний полученный mindboxid, прибавляем к нему 1-цу и устанавливаем
            # это значение в запрос который отправим на следующей итерации
            if len(values_list) == 1000:
                self.request_promo["page"]["firstMindboxId"] = values_list[-1]["ids"]["mindboxId"] + 1
                self.row_number += 1
                print (self.request_promo["page"]["firstMindboxId"])
                continue
            # если данных оказалось меньше 1000, значит больше данных нет, логируем этот факт и завершаем поток
            else:
                print('_________{}_:______complete___________'.format(self.name))
                l.info('Upload {} is complete. Last "firstMindboxId" is {}\n'.format(
                    self.name,
                    values_list[-1]["ids"]["mindboxId"])
                    )
                break


def main():
    All = Promo(methods["All"])
    All.insert()

    exec_sql('EXEC dwh.mindbox.Promo_upload')


if __name__ == '__main__':
    main()
