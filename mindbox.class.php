<?php
namespace MindBox;

use PHPDaemon\PubSub\PubSub;
class MindBox {
    
    public $operation = null;
    
    const URL = 'https://api.mindbox.ru/v3/operations/sync';
    
    const API_KEY = "MEA3c6hq4z2pJGKwll9k";
    const ENDPOINT = "zoloto585.processing";
    
    const DEBUG_API_KEY = "H9VMHdioi5NysfkCiua0";
    const DEBUG_ENDPOINT = "zoloto585.processing.test";
    
    const CREATE_USER = 'zoloto585CustomerRegistrationRTOM';
    const UPDATE_USER = 'zoloto585CustomerEditProfileRTOM';
    
    const CREATE_ORDER = 'zoloto585CreateOrderRTOM';
    
    const CREATE_AUTH_ORDER = 'zoloto585CreateOrderRTOMAuth';
    
    const EXECUTE_RTOM = 'zoloto585OrderChangeRTOM';
    
    const EXECUTE_INTEREST = 'zoloto585CustomerViewProductRTOM';
    
    private $api_key;
    private $endpoint;
    
    private $arr = null;
    
    // ������ � �������� 
    private $goods = [];
    
    public function __construct($is_debug = false)
    {
        if ($is_debug == true){
            $this->api_key = self::DEBUG_API_KEY;
            $this->endpoint = self::DEBUG_ENDPOINT;
        }
        else{
            $this->api_key = self::API_KEY;
            $this->endpoint = self::ENDPOINT;
        }
    }
    
    
    private function rawUser($data)
    {
        $arr = [];
        
        $sex = $data['gender'] == 'SEXF' ? 'f' : 'm';
        
        $city = $this->getCityAbbr($data['city_name']);
        $brand = $this->getBrand($data['city_name']);
        
        $arr['customer'] =
        [
            'customFields' =>[
                'zoloto585CustomerCity' => $city,
                'zoloto585CustomerBrand' => $brand
            ],
            'birthDate'=> $data['birth_date'],
            'lastName'=> $data['client_last_name'],
            'firstName' => $data['client_first_name'],
            'middleName' => $data['client_middle_name'],
            'area' =>[
                'ids' => [
                    'externalId' => $city
                ]
            ],
            'ids' => [
                'zoloto585CustomerGuid' => $data['client_id'],
            ],
         
            'email' => $data['email'],
            'sex' => $sex,
            'mobilePhone' => $data['mobile_phone']
        ];
        
        $arr["executionDateTimeUtc"] = date("d.m.Y H:i:s",time()-10800);
        return $arr;
    }
    
    public function updateUser($data)
    {
        $this->operation  = self::UPDATE_USER;
        $this->arr =$this->rawUser($data);
        $this->arr['customer']["subscriptions"] = [
        
            [
                "isSubscribed" => $data['do_not_use_sms'] == 1 ? false : true,
                "pointOfContact" => "SMS"
            ],
            [
                "pointOfContact" => "EMAIL",
                "isSubscribed" => $data['do_not_use_email']== 1 ? false : true,
            ]
        ];
    }
    
    public function createUser($data)
    {
        $this->operation  = self::CREATE_USER;
        $this->arr =$this->rawUser($data);
        
        if(!empty($data['card_number']))
             $this->arr["customer"]["discountCard"]["ids"]["number"] = $data['card_number'];
        
        $this->arr['customer']["subscriptions"] = [
                
                [
                    "isSubscribed" => $data['do_not_use_sms'] == 1 ? false : true,
                    "valueByDefault" => true,
                    "pointOfContact" => "SMS"
                ],
                [
                    "pointOfContact" => "EMAIL",
                    "isSubscribed" => $data['do_not_use_email']== 1 ? false : true,
                    "valueByDefault" => true
                ]
            ];
               
    }
      
    public function compliteOrder($retailCrmId)
    {
        $arr = [];
        $arr = [
                'ids' => ['zoloto585OrderRetailCrmId' => $retailCrmId],
        ];
        
        return $arr;
    }
    
    public function complitePOSOrder($uuid)
    {
        $arr = [];
        $arr = [
            'ids' => ['zoloto585OrderPOSId' => $uuid],
           
        ];
        
        return $arr;
    }
    
    public function prepareOrder($data, $offset)
    {
       
       if (empty($data['discountCards']) || !isset($data['discountCards'])) 
       {
          $softcheck = array_column($data['positions'], 'softchecknumber');
          if(!isset($softcheck[0]))
          {
              return;
          }
       }
        
       $order_type = null;
       $softcheck = [];
       $softcheck = array_column($data['positions'], 'softchecknumber');
       
       if (isset($softcheck[0]))
       {
           $sc = intval(substr($softcheck[0],1));
           if ($softcheck[0][0]==1)
           {
              $order_type = 1;
           }
           else
           {
               $order_type = 2;
               return [$order_type, $sc, []];
           }
       }
       else {
            // POS
           $order_type = 3;
           $sc = 0;
       }
        
       $arr = [];
       $arr['deliveryCost'] = 0;
       $arr['totalPrice'] = $data['amount'] - $data['discountamount']; // > 0 ? $data['amount'] : $arr['discountAmount'];
       
       $discounts = [];
       $shop_id = sprintf("4%'.03d\n", $data['shop']);
       
       
       if (isset($data['discounts']))
       {
           $dd = $data['discounts']['discount'];
           $darr = [];
           
           if( isset($dd[0])) {
               $darr = $dd;
           } else {
               $darr[0] = $dd;
           }
        
           foreach($darr as $disc)
           {
               $sap = $disc['goodCode'];
           
               switch($disc['advertType'])
               {
                   case 'DISCOUNT_GOODS':
                   case 'GOODS':
                   case 'DEFAULT':
                       $type =  'externalPromoAction';
                       break;
                    default:
                       $type = 'externalPromoAction';
                       break;
               }

               $discounts[$sap][] = [
                   'type' => $type,
                   'externalPromoAction' => [
                       'ids' => [
                           'externalId' => $disc['AdvertActExternalCode']
                       ]
                   ],
                   'amount' =>  $disc['amount']
               ];
               
               
               // �������� �� ���������
               // https://zoloto585.directcrm.ru/operations-log-entry/9f2a209d-c0fb-4b74-8a9c-c4e170e9e9bf
               // $disc['AdvertActExternalCode']
               $arr['discounts'][] = [
                   'type' => $type,
                   'externalPromoAction' => [
                       'ids' => [
                           'externalId' => $disc['AdvertActExternalCode']
                       ]
                   ],
                   'amount' =>  $disc['amount']
               ];
           }

       }
       
       $lines = [];
       
       foreach($data['positions'] as $item)
       {
           $line = [];
           $sap  = $item['goodscode'];
           $nulledsap = '00000000'.$sap;
           
           $uri = 'http://rtom.zoloto585.int:30013/v2/poses/0000/products/?sap_code='.$nulledsap;
           
           try {
               $dat = file_get_contents($uri);
           }
           catch (\Exception $e){
               echo "[ERROR]: {$e->getMessage()}\n";
               continue;
           }
           
           $sap_data = json_decode($dat, true);
           if ($sap_data)
           {
               if ($sap_data['code'] == 200)
               {
                   if (!isset($sap_data['data']['sap_code'])){
                       continue;
                   }
                   $xdata = $sap_data['data']['sap_code'][$nulledsap];
                   
                   $name = (!empty($xdata['product_name'])) ? $xdata['product_name'] : $xdata['name'];
                   $url = (!empty($xdata['image_url'])) ? $xdata['image_url']: "" ;
                   $price = $xdata['retail_price'];
                   $is_new = ($xdata['status_matrix'] == 3 && $xdata['status_order'] == 1) ? true : false;
                   
                   $line['product'] = [
                       'ids'=>[
                           'zoloto585System'=>$sap
                       ],
                       'name' => $name,
                       //'url' => $url,
                       // 'pictureUrl' => $url,
                       'price' =>  $item['amount']
                   
                   ];
                   $line['basePricePerItem'] = $item['cost'];
                   $line['quantity'] = intval($item['count']);
                    
                   // $line['customFields'] = ['zoloto585OrderPickupPoint'=> $shop_id];
                   if (isset($discounts[$sap])){
                       $line['discounts'] = $discounts[$sap];
                   }
                    
                   $lines[] = $line;
               }
           }
           else {
               continue;
           }
        
       
       }
       $arr['lines'] = $lines;
      
      
       $arr['area'] = [
           'ids'=> [
               'externalId' => $this->getCityAbbr($this->getCityName($shop_id))
           ]
       ];
       
       $oid = uniqid('',true);
       $arr['ids'] = [
           'zoloto585OrderPOSId' => $oid
       ];
      
       $ret = [ $order_type, $sc, $arr, $oid];
       return $ret;
        
        
        //$this->operation  = self::CREATE_ORDER;

    }
    
    /**
     * ���������� ������
     * @param array $data
     */
    public function populateGood($data)
    {
        
    }
    
    public  function getBrand($city)
    {
        $arr = CITY_CODES[array_search($city, array_column(CITY_CODES, 'city'))];
        if (isset($arr)){
            return $arr['brand'];
        } else {
            return 'NONE';
        }
        
    }
    
    public  function getCityName($code)
    {
        $arr = CITY_CODES[array_search($code, array_column(CITY_CODES, 'shopid'))];
        if (isset($arr)){
            return $arr['city'];
        } else {
            return 'NONE';
        }
    
    }
    
    public  function getCityAbbr($city)
    {
        $abbr = array_search($city, AREAS) ? array_search($city, AREAS) : '';
        return $abbr;
    }
    
    public function createRTOMOrder($arr){
        $this->operation = self::CREATE_AUTH_ORDER;
        $this->arr = $arr;
        $ret = $this->sendRequest();
        
        return $ret;
    }
    
    public function executeRTOMOrder($arr){
        $this->operation = self::EXECUTE_RTOM;
        $this->arr = $arr;
        $ret = $this->sendRequest();
        return $ret;
    }
    
    public function createRTOMInterest($arr)
    {
        $this->operation = self::EXECUTE_INTEREST;
        $this->arr = $arr;
        $ret = $this->sendRequest();
        return $ret;
    }
    
    public function sendRequest()
    {
        $uri = self::URL."?endpointId={$this->endpoint}&operation={$this->operation}";
       
        $ret = [
            "error" => false
        ];
        
        $jarr = json_encode($this->arr);

        $header = [];
        $header[] = 'Content-Type: application/json';
        $header[] = 'Accept: application/json';
        $header[] = 'Authorization: Mindbox secretKey="'.$this->api_key.'"';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jarr);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       
        $try = 0;
        $pass = false;
        do {
            $data_str  = curl_exec($ch);
            $status = curl_getinfo($ch);
            
            $ret["data"] = json_decode($data_str, true);
            if($status["http_code"] != 200)
            {
                $ret["error"] = true;
                $ret['data'] = null;
            }
            try{
                $pass = true;
                
            }
            catch (Exception $e){
                $try++;
            }
        } while (curl_errno($ch) && $try <= 3);
        
        return $ret;
    }
    
    // ���������� ��������� ������
    public function populateItem()
    {
        
    }
    
}