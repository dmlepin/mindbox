<?php
ini_set('memory_limit', '4GB');


require_once('mindbox.class.php');
require_once ('areas.php');

// ��������� SKU
$exclude_sku = [];

$conf = new RdKafka\Conf();

$conf->setRebalanceCb(function (RdKafka\KafkaConsumer $kafka, $err, array $partitions = null) {
    switch ($err) {
        case RD_KAFKA_RESP_ERR__ASSIGN_PARTITIONS:
            echo "Assign: ";
            var_dump($partitions);
            $kafka->assign($partitions);
            break;

        case RD_KAFKA_RESP_ERR__REVOKE_PARTITIONS:
            echo "Revoke: ";
            var_dump($partitions);
            $kafka->assign(NULL);
            break;

        default:
            throw new \Exception($err);
    }
});

$topicConf = new RdKafka\TopicConf();
$topicConf->set("auto.commit.interval.ms", 1e3);
$topicConf->set("offset.store.sync.interval.ms", 60e3);
$topicConf->set("offset.store.method", "file");
$topicConf->set("offset.store.path", "kafka_offset");
$topicConf->set("auto.offset.reset", 'smallest');


$rdk = new \RdKafka\Consumer();

$kafkaConnection = "PLAINTEXT://spb-pl-kfkapp01.zoloto585.int:9092, PLAINTEXT://spb-pl-kfkapp02.zoloto585.int:9092, PLAINTEXT://spb-pl-kfkapp03.zoloto585.int:9092";

$rdk->addBrokers($kafkaConnection);

$pos_topic_name = 'pos.purchases.json';

$topic = $rdk->newTopic($pos_topic_name, $topicConf);
$queue = $rdk->newQueue();

try {
    $topic->consumeQueueStart(0, RD_KAFKA_OFFSET_STORED, $queue);
}
catch (Exception $te){
    echo "RDF: {$te->getMessage()}\n";   
}

use MindBox\MindBox;

while (true)
{
    $msg = $queue->consume(1000);

    if (is_object($msg))
    {
        if ($msg->err)
        {
            switch ($msg->err)
            {
                case RD_KAFKA_RESP_ERR_NO_ERROR:
                    var_dump($msg);
                    break;
                case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                    echo "No more messages; will wait for more\n";
            }
        }
        else
        {
            if(!empty($msg->payload))
            {
                $offset = $msg->offset;
                $payload = $msg->payload;

                $arr =  json_decode($payload, true);
                $mb = new MindBox();
                // create MindBox Object
                try
                {
                    $aret = $mb->prepareOrder($arr, $offset);
                                       
                    if ($aret[0] == 1)
                    {
                        $oid = $aret[1];
                        $ciod = $aret[3];
                        
                        $user_uuid_u = 'http://rtom.zoloto585.int:30000/v1/orders/'.$oid;
                        $uuid_arr = json_decode(file_get_contents($user_uuid_u), true);
                        
                        if (isset($uuid_arr['code']) && $uuid_arr['code']==404) {
                            file_put_contents('rtom_oids_errors', "{$offset}\\norder: {$oid}\n\n", FILE_APPEND);
                            continue;
                        }
                        
                        $uuid = $uuid_arr['data']['client'];
                        
                        $user_uri = 'http://rtom.zoloto585.int:30013/v2/clients/'.$uuid;
                        try {
                            // @ Not GOOD IDEA!
                            $user_uri_data = @file_get_contents($user_uri);
                        }
                        catch (Exception $e){
                           echo $e->getMessage();
                           echo "\n";
                           continue;
                        }
                        $user_arr = json_decode($user_uri_data, true);
                       
                        if ($user_arr['code'] == 200)
                        {
                            $customer = [
                                'discountCard' => [
                                    'ids' => [
                                        'number' => $user_arr['data']['card_number']
                                    ]
                                ],
                                'mobilePhone' => $user_arr['data']['mobile_phone']
                            ];
                            
                            $final = [];
                            $final['customer'] = $customer;
                            $final['order'] = $aret[2];
                            $final['executionDateTimeUtc'] = date("d.m.Y H:i:s",time()-10800);
                            
                            $ret = $mb->createRTOMOrder($final);
                            file_put_contents('rtom_to_mb', "{$offset}\n{$ciod}\n".json_encode($ret)."\n\n", FILE_APPEND);
                           
                            // 400
                            
                            if ($ret['data']['status'] == 'Success')
                            {
                                file_put_contents('rtom_oids', "{$offset}\n{$ciod}\n".json_encode($ret)."\n\n", FILE_APPEND);
                                if ($ret['data']['order']['processingStatus']=='Processed' && $ret['data']['customer']['processingStatus']=='Found')
                                {
                                    $final = [];
                                    
                                    echo "RTOM\t{$offset}";
                                    echo "\texec: {$ciod}\n";
                                    
                                    $dat = $mb->complitePosOrder($ciod);
                                    $final['order'] = $dat;
                                    $final['orderLinesStatus'] = "f";
                                    $final['executionDateTimeUtc'] = date("d.m.Y H:i:s",time()-10800);
                                    
                                    $ret = $mb->executeRTOMOrder($final);
                                }
                            }
                        }
                        else {
                            file_put_contents('rtom_oids_errors', "{$offset}\nuser: {$uuid}\n\n", FILE_APPEND);
                        }
                       //$mb->execute();
                    }
                    
                    //site
                    if ($aret[0] == 2)
                    {
                        $retailCrmID = $aret[1];
                        continue;
                        /*
                        $dat = $mb->compliteSiteOrder($retailCrmID);
                        $final['order'] = $dat;
                        $final['executionDateTimeUtc'] = date("d.m.Y H:i:s",time()-10800);
                        $mb->execute();
                        */
                       // file_put_contents('site.offsets', "{$retailCrmID}\n", FILE_APPEND);
                    }
                    
                    // pos
                    if ($aret[0] == 3)
                    {
                        //echo "POS\t{$offset}\n";
                        continue;
                    }
                }
                catch (Exception $e)
                {
                    // ��������� �� ������������ ���c���
                    file_put_contents('nwo.txt', $payload."\n\n", FILE_APPEND);
                    echo $e->GetMessage();
                }
            }
        }
    }
}